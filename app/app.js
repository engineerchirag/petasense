/**
 *
 * AngularJS Boilerplate
 * @description           Description
 * @author                Jozef Butko // www.jozefbutko.com/resume
 * @url                   www.jozefbutko.com
 * @version               1.1.7
 * @date                  March 2015
 * @license               MIT
 *
 */
;(function() {


  /**
   * Definition of the main app module and its dependencies
   */
  angular
    .module('boilerplate', [
      'ui.router',
      'ngLodash'
    ])
    .config(config);

  // safe dependency injection
  // this prevents minification issues
  config.$inject = ['$urlRouterProvider', '$stateProvider', '$locationProvider', '$httpProvider', '$compileProvider'];

  /**
   * App routing
   *
   * You can leave it here in the config section or take it out
   * into separate file
   *
   */
  function config($urlRouterProvider, $stateProvider, $locationProvider, $httpProvider, $compileProvider) {

    $locationProvider.html5Mode(false);

    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/home/home.view.tpl.html',
        controller: 'HomeController',
        controllerAs: 'home'
      })
      .state('vibrations', {
        url: '/vibrations',
        templateUrl: 'app/vibrations/vibrations.view.tpl.html',
        controller: 'VibrationsController',
        controllerAs: 'vibrations',
        resolve: {
          login: ['$in', function($in){
            return {};
              // return $in.local().Auth.login();
          }]
        }
      });

    $httpProvider.interceptors.push('authInterceptor');
    $httpProvider.defaults.withCredentials = true;

  }

  /**
   * You can intercept any request or response inside authInterceptor
   * or handle what should happend on 40x, 50x errors
   *
   */
  angular
    .module('boilerplate')
    .factory('authInterceptor', authInterceptor);

  authInterceptor.$inject = ['$rootScope', '$q', '$location'];
  function authInterceptor($rootScope, $q, $location) {

    return {
      // intercept every request
      request: function(config) {
        config.headers = config.headers || {};
        return config;
      },
      response: function(res) {
        return res;
      },
      // Catch 404 errors
      responseError: function(response) {
        if (response.status === 404) {
          $location.path('/');
          return $q.reject(response);
        } else {
          return $q.reject(response);
        }
      }
    };
  }

  /**
   * Run block
   */
  angular
    .module('boilerplate')
    .run(run);

  run.$inject = ['$rootScope', '$location'];
  function run($rootScope, $location) {
  }
})();

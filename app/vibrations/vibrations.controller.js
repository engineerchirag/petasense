;(function() {

  angular
    .module('boilerplate')
    .controller('VibrationsController', VibrationsController);

  VibrationsController.$inject = ['$scope', '$in', 'lodash'];
  function VibrationsController($scope, $in, lodash) {

    $scope.tabs = [{
        axis: 'x',
        visibility: 1
      },{
        axis: 'y',
        visibility: 1
      },{
        axis: 'z',
        visibility: 1
      }];

    $scope.showVibrations = function(axis){
      var axisIndex = lodash.findIndex($scope.tabs, {'axis': axis});
      $scope.tabs[axisIndex].visibility = $scope.tabs[axisIndex].visibility ? 0 : 1;
      d3.select("#chart .line-" + axis).style("opacity", $scope.tabs[axisIndex].visibility);
    };
  }
})();

;(function() {

  'use strict';
  angular
    .module('boilerplate')
    .directive('lineGraph', lineGraph);

  function lineGraph() {

    var controller = ['$scope', '$in', function ($scope, $in) {

      $scope.tabs.forEach(function(tab){
        $in.local().Vibration.getVibrationData(tab.axis).then(function(data){
          data = $in.local().Vibration.formatAndSortedDataForLineGraph(data);
          createGraph(data, tab.axis);
        });
      });

      var margin = {
        top: 20,
        right: 20,
        bottom: 20,
        left: 45
      };
      var width = window.innerWidth - 140 - margin.left - margin.right,
        height = 250 - margin.top - margin.bottom,
        bisectDate = d3.bisector(function(d) { return d.date; }).left;

      var currentDate = new Date();
      var previousDate = new Date();
      previousDate.setFullYear(previousDate.getFullYear() - 1);
      var x = d3.time.scale()
        .domain([previousDate, currentDate])
        .range([0, width]);

      var y = d3.scale.linear()
        .domain([0, 1])
        .range([height, 0]);

      var line = d3.svg.line()
        .interpolate("basis")
        .x(function (d) {return x(d.date);})
        .y(function (d) {return y(d.value);});

      var zoom = d3.behavior.zoom()
        .x(x)
        .on("zoom", zoomed);

      var svg = d3.select('#chart')
        .append("svg:svg")
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append("svg:g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
        .call(zoom);

      svg.append("svg:rect")
        .attr("width", width)
        .attr("height", height)
        .attr("class", "plot");

      var make_x_axis = function () {
        return d3.svg.axis()
          .scale(x)
          .orient("bottom")
          .ticks(5);
      };

      var make_y_axis = function () {
        return d3.svg.axis()
          .scale(y)
          .orient("left")
          .ticks(5);
      };

      var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom")
        .ticks(5);

      svg.append("svg:g")
        .attr("class", "x axis")
        .attr("transform", "translate(0, " + height + ")")
        .call(xAxis);

      var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left")
        .ticks(5);

      svg.append("g")
        .attr("class", "y axis")
        .call(yAxis);

      svg.append("g")
        .attr("class", "x grid")
        .attr("transform", "translate(0," + height + ")")
        .call(make_x_axis()
          .tickSize(-height, 0, 0)
          .tickFormat(""));

      svg.append("g")
        .attr("class", "y grid")
        .call(make_y_axis()
          .tickSize(-width, 0, 0)
          .tickFormat(""));

      var clip = svg.append("svg:clipPath")
        .attr("id", "clip")
        .append("svg:rect")
        .attr("x", 0)
        .attr("y", 0)
        .attr("width", width)
        .attr("height", height);

      var chartBody = svg.append("g")
        .attr("clip-path", "url(#clip)");

      var focus = svg.append("g")
        .attr("class", "focus")
        .style("display", "none");

      focus.append("line")
        .attr("class", "x-hover-line hover-line")
        .attr("y1", 0)
        .attr("y2", height);

      focus.append("line")
        .attr("class", "y-hover-line hover-line")
        .attr("x1", width)
        .attr("x2", width);

      focus.append("circle")
        .attr("r", 7.5);

      focus.append("text")
        .attr("x", 15)
        .attr("dy", ".31em")
        .attr('class', 'tool-tip');

      function mousemove(data) {
        console.log(d3.mouse(this));
        var x0 = x.invert(d3.mouse(this)[0]),
          i = bisectDate(data, x0, 1),
          d0 = data[i - 1],
          d1 = data[i],
          d = x0 - d0.date > d1.date - x0 ? d1 : d0;
        focus.attr("transform", "translate(" + x(d.date) + "," + y(d.value) + ")");
        focus.select("text").text(function() { return d.value + ', '+  new Date(d.date).toDateString(); });
        focus.select(".x-hover-line").attr("y2", height - y(d.value));
        focus.select(".y-hover-line").attr("x2", width + width);
      }

      function highlight(obj, axis){
        d3.selectAll(".line").style('opacity', 0.3);
        d3.select(".line-" + axis).style('opacity', 1);
      }

      function createGraph(data, axis){
        chartBody.append("svg:path")
          .datum(data)
          .attr("class", "line line-"+ axis + " color-" + axis)
          .attr("d", line)
          .on("mouseover", function(obj){
            // highlight(obj, axis);
            focus.style("display", null);
          })
          .on("mouseout", function(){
            d3.selectAll(".line").style('opacity', 1);
            focus.style("display", "none");
          })
          .on("mousemove", mousemove);
      }

      function zoomed() {
        svg.select(".x.axis").call(xAxis);
        svg.select(".y.axis").call(yAxis);
        svg.select(".x.grid")
          .call(make_x_axis()
            .tickSize(-height, 0, 0)
            .tickFormat(""));
        svg.select(".y.grid")
          .call(make_y_axis()
            .tickSize(-width, 0, 0)
            .tickFormat(""));
        $scope.tabs.forEach(function(tab){
          svg.select(".line-"+ tab.axis)
            .attr("class",  "line line-"+ tab.axis + " color-" + tab.axis)
            .attr("d", line);
        });
      }

      function resize(){
        var width = parseInt(d3.select("#chart").style("width")) - margin.left - margin.right,
          height = parseInt(d3.select("#chart").style("height")) - margin.top - margin.bottom;
        x.range([0, width]);
        y.range([height, 0]);
        d3.select("#chart svg").attr('width', width + margin.left + margin.right);

        // Update the axis and text with the new scale
        svg.select('.x.axis')
          .attr("transform", "translate(0," + height + ")")
          .call(x);

        svg.select('.y.axis')
          .call(y);
      }

      d3.select(window).on('resize', resize);
      resize();
    }];

    // Definition of directive
    var directiveDefinitionObject = {
      restrict: 'AE',
      templateUrl: 'common/directives/line-graph/line-graph.html',
      scope: {
        tabs: '=?'
      },
      controller: controller
    };

    return directiveDefinitionObject;
  }

})();

;(function() {
  'use strict';
  angular
    .module('boilerplate')
    .factory('Vibration', [
      '$window', '$rootScope', VibrationModel
    ]);
    function VibrationModel($window, $rootScope) {

      function Vibration(id) {
        this.id = id;
      }
      Vibration.build = function (data) {
        return new Vibration(
          data.id
        );
      };
      Vibration.buildCollection  = function(collection) {
        return angular.isArray(collection) ? collection.map(Vibration.build) : [];
      };

      Vibration.apiResponseTransformer = function (responseData) {
        if (angular.isArray(responseData)) {
          return responseData.map(Vibration.build);
        }
        return Vibration.build(responseData);
      };
      return Vibration;
    }

})();

;(function() {
  'use strict';
  angular
    .module('boilerplate')
    .factory('AuthLocalService', [
      '$in', AuthService
    ]);
  function AuthService($in) {
    return {
      login: function (){
        return $in.remote().Auth.login().then(function(){
        	//Get cookies or session from Headers and set to session variable for next subsequent api request.
        });
      }
    };
  }
})();

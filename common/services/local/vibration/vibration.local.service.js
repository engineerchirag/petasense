;(function() {
  'use strict';
  angular
    .module('boilerplate')
    .factory('VibrationLocalService', [
      '$in', VibrationService
    ]);
  function VibrationService($in) {
    return {
      logger: function logger(data){
        return console.log(data);
      },
      formatAndSortedDataForLineGraph: function formatAndSortedDataForLineGraph(data){
        return data.trend_data.data.map(function (_, idx) {
          return { value: data.trend_data.data[idx], date: new Date(data.trend_data.time[idx]).getTime()};
        }).sort(function(a,b){
          return a.date - b.date;
        });
      },
      getVibrationData: function(axis){
        return $in.remote().Vibration.getVibrationData(axis);
      }
    };
  }
})();

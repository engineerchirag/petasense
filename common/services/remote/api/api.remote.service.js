;(function() {
  'use strict';
  angular
    .module('boilerplate')
    .factory('ApiRemoteService', [
      '$http', '$q', '$in', 'CONSTANTS', ApiService
    ]);
  function ApiService($http, $q, $in, CONSTANTS) {
    function api(method, url, params, data){
      var deferred = $q.defer();
      var config = {
        method: method,
        url: CONSTANTS.API_URL + url,
        params: params,
        data: data
      };

      $http(config).then(function(data) {
        if (!data.config) {
          console.log('Server error occured.');
        }
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
      return deferred.promise;
    }
    return {
      post: function (url, params, data){
        return api('POST', url, params, data);
      },
      get: function (url, params){
        return api('GET', url, params);
      }
    };
  }
})();

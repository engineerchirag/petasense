;(function() {
  'use strict';
  angular
    .module('boilerplate')
    .factory('AuthRemoteService', [
      '$http', '$q', '$in', 'CONSTANTS', AuthService
    ]);
  function AuthService($http, $q, $in, CONSTANTS) {
    return {
      login: function (){
        var url = 'common/json/login.json';
        // var url = 'login';   To login through remote API call
        var params = {};
        var data = {
          "username": "test@petasense.com",
          "password": "pYEMZU3ggJj9u7XD"
        };
        return $in.remote().API.post(url, params, data).then(function(data){
        });
      }
    };
  }
})();

;(function() {
  'use strict';
  angular
    .module('boilerplate')
    .factory('VibrationRemoteService', [
      '$http', '$q', '$in', 'CONSTANTS', VibrationService
    ]);
    function VibrationService($http, $q, $in, CONSTANTS) {
      return {
        getVibrationData: function(axis) {
          var url = 'common/json/'+axis+'.json';
          // var url = 'webapp/vibration-data/​129/broadband-trend '; To call graph data from remote
          var params = {
            axis: axis
          };
          return $in.remote().API.get(url, params).then(function(res){
            return res.data;
          });
        }
      }
    }
})();

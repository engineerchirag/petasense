;(function() {
  'use strict';
  angular
    .module('boilerplate')
    .factory('$in', [
      '$injector', InjectorService
    ]);
    function InjectorService($injector) {
        return {
          remote : function(){
            return {
              API: $injector.get('ApiRemoteService'),
              Auth: $injector.get('AuthRemoteService'),
              Vibration: $injector.get('VibrationRemoteService')
            }
          },
          local: function(){
            return {
              Auth: $injector.get('AuthLocalService'),
              Vibration: $injector.get('VibrationLocalService')
            }
          },
          model:  function(){
            return{
              Vibration: $injector.get('VibrationModel')
            }
          },
          storage: {},
          utils: {},
          const: {}
        };
    }
})();
